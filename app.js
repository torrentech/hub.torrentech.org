require("dotenv").config(); //since app.js is in the same dir as .env, path to it is not needed?
//console.log(process.env);

const express = require("express");
const mongoose = require("mongoose");
const PORT = 5000;
const authRoutes = require("./routes/users");
const dataRoutes = require("./routes/releases");

mongoose
.connect(process.env.MONGO_URI, {
  dbName: "Torrentech", //why this one is needed when Torrentech database is already in MONGO_URI?
  useNewUrlParser: true,
  useUnifiedTopology: true
})
.then(() => {
  console.log("Database connection Success.");
})
.catch((err) => {
  console.error("Mongo Connection Error", err);
});

const app = express();

// CORS
app.use((req, res, next) => {
  if (process.env.VERSION == "dev") {
    // development version with no control of origin
    res.header("Access-Control-Allow-Origin", "*");
  }
  else {
    // production version with control of origin
    res.header("Access-Control-Allow-Origin", "https://torrents.torrentech.org");
  }

  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header("Access-Control-Allow-Methods", "OPTIONS, GET, POST, PUT, PATCH, DELETE");

  if (req.method === "OPTIONS") {
    return res.sendStatus(200);
  }
  next();
});

/* Added console output of request, may be removed */
app.use((req, res, next) => {
  var currentDate = new Date();
  var time = currentDate.getHours() + ":" + currentDate.getMinutes() + ":" + currentDate.getSeconds();  
  console.log(`console.log from app.js: ${time}, method: ${req.method}, body: ${req.body}, url: ${req.url}`);
  next();
});

app.use(express.json()); //This method returns the middleware that only parses JSON and only looks at the requests where the content-type header matches the type option.
app.use(express.urlencoded({ extended: true }));

app.get("/ping", (req, res) => {
  return res.send({
    error: false,
    message: "Server is healthy",
  });
});

app.use("/users", authRoutes);
app.use("/releases", dataRoutes);

app.listen(PORT, () => {
  console.log("Server started listening on PORT : " + PORT);
});
