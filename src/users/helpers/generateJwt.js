const jwt = require("jsonwebtoken");
const options = {
  expiresIn: "240h",
};

async function generateJwt(userId, username) {
  try {
    const payload = { id: userId, username: username };
    const token = await jwt.sign(payload, process.env.JWT_SECRET, options);
    return { error: false, token: token };
  } catch (error) {
    return { error: true };
  }
}

module.exports = { generateJwt };
