const nodemailer = require("nodemailer");

async function sendEmail(email, code) {
  try {
    if (process.env.VERSION == "dev") {
      var smtpEndpoint = "smtp.mailtrap.io";
      var port = 2525;
      var senderAddress = "NAME <ADDRESS>";
      var smtpUsername = "xxxxxxxxxxxxx";
      var smtpPassword = "xxxxxxxxxxxxx";
    }
    else {
      var smtpEndpoint = "localhost";
      var port = 25;
      var senderAddress = "Torrentech <noreply@torrentech.org>";
    }
    let toAddress = email;
    let subject = "Verify your email";

    // The body of the email for recipients
    var body_html = `<!DOCTYPE> 
    <html>
      <body>
        <p>Your authentication/token code is: </p> <b>${code}</b>
      </body>
    </html>`;

    // Create the SMTP transport. Dev version has to SMTP auth to Mailtrap. Prod version connects to local sendmail unauthenticated.
    if (process.env.VERSION == "dev") {
      var transporter = nodemailer.createTransport({
        host: smtpEndpoint,
        port: port,
        secure: false, // true for 465, false for other ports
        auth: {
          user: smtpUsername,
          pass: smtpPassword
        }
      });
    }
    else {
      var transporter = nodemailer.createTransport({
        host: smtpEndpoint,
        port: port,
        secure: false // true for 465, false for other ports
      });
    }

    // Specify the fields in the email.
    let mailOptions = {
      from: senderAddress,
      to: toAddress,
      subject: subject,
      html: body_html,
    };

    let info = await transporter.sendMail(mailOptions);
    // console.log("sendMail info" + info);
    return { error: false };
  } catch (error) {
    console.error("send-email-error", error);
    return {
      error: true,
      message: "Cannot send email",
    };
  }
}

module.exports = { sendEmail };