const mongoose = require("mongoose");
const TagSchema = require('../../schemas/tag');
const Tags = mongoose.model("tag", TagSchema);
module.exports = Tags;