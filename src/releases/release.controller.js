const Release = require("./release.model");
const Magnet = require("./magnet.model");
const Tag = require("./tags.model");
const ReleaseDetail = require("./releasedetail.model");
const Infohashes = require('../../util/deleteInfohashes.js');

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

exports.Tags = async (req, res) => {
  Tag
    .find({})
    .select('name type -_id')
    .then(function (results) {
      return res.send(results);
    })
    .catch(function (err) {
      return res.status(500).json({
        error: true,
        message: "Error when retrieving records",
      })
    });
}

exports.Details = async (req, res) => {
  let myQuery;
  myQuery = [{
    '$match': {
      'release_id': req.body.release_id
    }
  }, {
    '$lookup': {
      from: 'releases_details',
      localField: 'release_id',
      foreignField: 'release_id',
      as: 'details'
    }
  }, {
    $limit: 1
  }];
  Release
    .aggregate(myQuery)
    .then(function (results) {
      return res.send(results);
    })
    .catch(function (err) {
      return res.status(500).json({
        error: true,
        message: "Error when retrieving records",
      })
    });
}

exports.Update = async (req, res) => {
  //let myQuery;
  //myQuery = [{ 'release_id': req.body.release_id }, { 'artist' : req.body.asrtist }];
  console.log(req);
  Release
    .findOneAndUpdate({
      "_id": req.body._id
    }, {
      $set: {
        "_id": req.body._id,
        "release_id": req.body.release_id,
        "artist": req.body.artist,
        "user_id_update": req.body.user_id,
        "remark": req.body.remark,
        "name": req.body.name,
        "release_year": req.body.release_year,
        "release_month": req.body.release_month,
        "release_day": req.body.release_day,
        "label": req.body.label,
        "weblink": req.body.weblink,
        "tags": req.body.tags
      }
    }, {
      new: true,
      upsert: false,
      useFindAndModify: false
    })
    .then(function (results) {
      console.log(results);
      return res.send(results);
    })
    .catch(function (err) {
      return res.status(500).json({
        error: true,
        message: "Error when retrieving records",
      })
    });
}

async function FindInfoHashes(release_id) {
  let fih;
  fih = await Magnet
  .find({release_id: release_id})
  .select('infohash -_id')
  .exec();
  return fih;
}

exports.DeleteRelease = async (req, res) => {
  let out;
  out = await FindInfoHashes(req.body.release_id);
  values = out.map(a => a.infohash);
  let ReleaseDeleted;
  let DetailsDeleted;
  let MagnetsDeleted;
  let r3;

  // delete from release collection
  Release
    .deleteOne({
      "_id": req.body._id
    })
    .then(function (results) {
      ReleaseDeleted = results.deletedCount;
    })
    .catch(function (err) {
      return res.status(500).json({
        error: true,
        message: "Error when retrieving records for Releases",
        status: "Not OK"
      })
    });

  //delete from release details collection
  ReleaseDetail
    .deleteOne({
      "release_id": req.body.release_id
    })
    .then(function (results) {
      DetailsDeleted = results.deletedCount;;
    })
    .catch(function (err) {
      return res.status(500).json({
        error: true,
        message: "Error when retrieving records for Releases details",
        status: "Not OK"
      })
    });

  Magnet
    .deleteMany({
      release_id: req.body.release_id
    })
    .then(function (results) {
      MagnetsDeleted = results.deletedCount;
      r3 = {
        ReleaseDeleted,
        DetailsDeleted,
        MagnetsDeleted
      }
      r3 = JSON.stringify(r3);
      console.log(r3);
      return res.send(r3);
    })
    .catch(function (err) {
      return res.status(500).json({
        error: true,
        message: "Error when retrieving records for Magnets",
        status: "Not OK"
      })
    });

  // imported from deleleteInfohashes.js
  Infohashes.deleteInfohashes(values);
}

exports.DeleteMagnet = async (req, res) => {
  let r3;
  Magnet
    .deleteOne({infohash: req.body.infohash_id})
    .then(function (results) {
    MagnetsDeleted = results.deletedCount;
      r3 = JSON.stringify(MagnetsDeleted);
      return res.send(r3);
     })
    .catch(function (err) {
      return res.status(500).json({
        error: true,
        message: "Error when retrieving records for Magnets",
        status: "Not OK"
      })
    });
  Infohashes.deleteInfohashes([req.body.infohash_id]);
}


exports.UpdateDetails = async (req, res) => {
  ReleaseDetail
    .findOneAndUpdate({
      "release_id": req.body.release_id
    }, {
      $set: {
        "_id": req.body._id,
        "release_id": req.body.release_id,
        // "cover": req.body.cover,
        "tracklist": req.body.tracklist
      }
    }, {
      new: true,
      upsert: false,
      useFindAndModify: false
    })
    .then(function (results) {
      console.log(results);
      return res.send(results);
    })
    .catch(function (err) {
      return res.status(500).json({
        error: true,
        message: "Error when retrieving records",
      })
    });
}

exports.Search = async (req, res) => {
  let myQuery;
  let sortVar;

  //await sleep(1000);
  //there was no search, loading all by default, uncomment projection field if you want score to be included in resultset
  if (req.body.search == undefined || req.body.search == '') {
    sortVar = {
      release_year: 'desc',
      release_month: 'desc',
      release_day: 'desc',
      artist: 'asc'
    };
    myQuery = {};
  }
  //if there was a search, there is score field too sent
  else {
    sortVar = {
      score: {
        $meta: "textScore"
      },
      release_year: 'desc',
      release_month: 'desc',
      release_day: 'desc',
      artist: 'asc'
    };
    myQuery = {
      $text: {
        $search: req.body.search
      }
    };
  }

  const options = {
    page: req.body.pg,
    limit: 100,
    sort: sortVar,
  };

  Release
    .paginate(myQuery, options)
    .then(function (results) {
      return res.send(results);
    })
    .catch(function (err) {
      return res.status(500).json({
        error: true,
        message: "Error when retrieving records",
      })
    });
};

exports.Torrent = async (req, res) => {
  Magnet
    .find({
      release_id: req.body.release_id
    })
    .then(function (results) {
      return res.send(results);
    })
    .catch(function (err) {
      return res.status(500).json({
        error: true,
        message: "Error when retrieving records",
      })
    });
}


//search without pagination
// exports.Get = async (req, res) => {
//   try {
//     const search = req.body.search;
//     await Release
//     .find(
//         { $text : { $search: search } },
//         { score : { $meta: "textScore" } }
//     )
//     .lean()
//     .sort({ score : { $meta : 'textScore' } })
//     .exec(function(err, searchOutput) {
//         if (err) { console.log(err); }
//         else { return res.send(searchOutput); }
//     });
//   } catch (error) {
//     return res.status(500).json({
//       error: true,
//       message: "Error when retrieving records",
//     });
//   }
// };
