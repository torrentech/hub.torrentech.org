const mongoose = require("mongoose");
//another version of pagination plugin with same returning parameters: https://github.com/lucaiaconelli/mongoose-paginate-ts
let aggregatePaginate = require("mongoose-paginate-v2");
const releaseSchema = require('../../schemas/release');
releaseSchema.plugin(aggregatePaginate);
const Release = mongoose.model("release", releaseSchema);
module.exports = Release;