const mongoose = require("mongoose");
const magnetSchema = require('../../schemas/magnet');
const Magnets = mongoose.model("magnet", magnetSchema);
module.exports = Magnets;