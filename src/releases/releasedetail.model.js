const mongoose = require("mongoose");
const releaseDetailSchema = require('../../schemas/release_detail');
const ReleaseDetail = mongoose.model("releases_detail", releaseDetailSchema);
module.exports = ReleaseDetail;