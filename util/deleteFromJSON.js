(async () => {
  const path = require('path');
  require("dotenv").config({ path: path.resolve(__dirname, '../.env') });
  
  const deleteHashes = require('./deleteInfohashes.js');
  
  const fs = require('fs');
  const incompletesFile = fs.readFileSync('/home/pyc/1/incomplete.txt', 'utf8');
  const incompletesObject = JSON.parse(incompletesFile);
  
  console.log("Number of incomplete magnets from incomplete.txt: ", incompletesObject.length);
  
  let incompletesArray = [];
  for (let i = 0; i < incompletesObject.length; i++) { incompletesArray[i] = incompletesObject[i].hash.toLowerCase(); }
  
  console.log("Whitelist path:", process.env.WHITELIST_PATH);
  
  //deleting from whitelist.txt
  try {
    let out = await deleteHashes.deleteInfohashes(incompletesArray);
    console.log("Number of deleted hashes from whitelist.txt:", out);
  } catch (error) {
    console.log("Error in number of deleted hashes from whitelist.txt", error);
  }
      
  const mongoose = require('mongoose');
  const MagnetSchema = require('../schemas/magnet');
  const MagnetModel = mongoose.model('magnet', MagnetSchema);
  const ReleaseSchema = require('../schemas/release');
  const ReleaseModel = mongoose.model('release', ReleaseSchema);
  const ReleaseDetailSchema = require('../schemas/release_detail');
  const ReleaseDetailModel = mongoose.model('releases_detail', ReleaseDetailSchema);

  try {
      await mongoose.connect(process.env.MONGO_URI, {dbName: "Torrentech", useNewUrlParser: true, useUnifiedTopology: true});
      console.log("Connected to database");
  }
  catch (error) {
      console.log("Could not connect to database");
      console.log(error);
      process.exit(1);
  }

  //just listing records for deletion
  //const records = await MagnetModel.find({ 'infohash': { $in: incompletesArray } });
  //console.log(records);

  //actually deleting records
  const result = await MagnetModel.deleteMany({ infohash:{ "$in": incompletesArray} });
  console.log("Result of deletion from magnets collection: ", result);

  try {
    let nrReleasesWithoutMagnets = 0;
    let deletedReleases = 0;
    let deletedReleasesDetails = 0;
    let allReleases = await ReleaseModel.find({}).exec()
      for (let i = 0; i < allReleases.length; i++) {
          try {
            magnetsForRelease = await MagnetModel.find({release_id: allReleases[i].release_id });
            if (magnetsForRelease.length == 0) {
                nrReleasesWithoutMagnets++;
                //console.log("release_id for which there are no magnets: ", allReleases[i].release_id);
                ReleaseModel.deleteOne ({release_id: {$eq: allReleases[i].release_id}}).then( () => { deletedReleases++; });
                ReleaseDetailModel.deleteOne ({release_id: {$eq: allReleases[i].release_id}}).then( () => { deletedReleasesDetails++; });                
            }
          } catch (error) {
              console.log("ERROR 1");
          }
      };
    console.log('Number of releases without magnets: ', nrReleasesWithoutMagnets);
    console.log('Number of deleted releases: ', deletedReleases);
    console.log('Number of deleted releases_details: ', deletedReleasesDetails);
  } catch (error) {
        console.log("ERROR 2");
  }
})().then(() => {process.exit();});