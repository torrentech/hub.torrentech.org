const mongoose = require('mongoose');
//Weird trip is that it doesn't matter if not all fields are in ./schemas/releaseArtistAlbum, find returns everything if projection is omitted
const MagnetSchema = require('../schemas/magnet');
const MagnetModel = mongoose.model('magnet', MagnetSchema);
const fetch = require('cross-fetch');

let fetchRes;
let scrapeData;
let arrayData = [];
let infohashData;
let query;
let magnet_doc;

(async () => {
    try {
        // info_hash:seed_count:peer_count:download_count 
        fetchRes = await fetch('http://localhost:6969/stats?mode=tpbs&format=txtp');
        if (fetchRes.status >= 400) throw new Error("Bad response from server");
        scrapeData = await fetchRes.text();
        arrayData = scrapeData.split('\n');
        // console.log("arrayData:");
        // console.log(arrayData);
    } catch (error) {
        console.log("Can't contact tracker, or the data it sent is broken");
        console.log(error);
        process.exit(1);
    }

    try {
        await mongoose.connect("mongodb://localhost", {dbName: "Torrentech", useNewUrlParser: true, useUnifiedTopology: true});
        console.log("Connected to database");
    }
    catch(error) {
        console.log("Could not connect to database");
        console.log(error);
        process.exit(1);
    }    

    //rotates all scrapes, finds infohashes in magnets and update seeders & leechers
    try {
        for (let i = 0; i < arrayData.length-1; i++) {
            infohashData = arrayData[i].split(':');
            // console.log("infohashData:");
            // console.log(infohashData);
            query = { infohash: infohashData[0].toLowerCase() };
            magnet_doc = await MagnetModel.findOne(query);
            // console.log("magnet_doc:");
            // console.log(magnet_doc);

            magnet_doc.seeders = infohashData[1];
            magnet_doc.leechers = infohashData[2];

            if (infohashData[1] == 0) {
                if (magnet_doc.timestamp_no_seeds == null) magnet_doc.timestamp_no_seeds = Date.now();
            }
            else magnet_doc.timestamp_no_seeds = null;

            let res = await magnet_doc.save();
            console.log("res:");
            console.log(res);
        }
    } catch (error) {
        console.log("Can't fill magnet docs with seeders and leechers data");
        console.log(error);
        process.exit(1);
    }
})().then(() => {process.exit();});