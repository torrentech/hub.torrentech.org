const replace = require('replace-in-file');
//const path = require('path');
//require("dotenv").config({ path: path.resolve(__dirname, '../.env') });

function execShellCommand(cmd) {
    const exec = require("child_process").exec;
    return new Promise((resolve, reject) => {
      exec(cmd, { maxBuffer: 1024 * 500 }, (error, stdout, stderr) => {
        if (error) {
          console.warn(error);
        } else if (stdout) {
          console.log(stdout); 
        } else {
          console.log(stderr);
        }
        resolve(stdout ? true : false);
      });
    });
  }

exports.deleteInfohashes = async (infohashes) => {
  let nrDeletedInfohashes = 0;
  for (let i = 0; i < infohashes.length; i++) {
      let options = {
        files: process.env.WHITELIST_PATH,
        from: infohashes[i] + "\n",
        to: ""
      };
      try {
          const result = await replace(options);
          console.log("infohash", infohashes[i]);
          // console.log(".env: ", process.env.WHITELIST_PATH); //this should be read from .env file, because require("dotenv").config(); exists, otherwise it is read from system variables
          console.log('Replacement result:', result); 
          if (result[0].hasChanged) nrDeletedInfohashes++;
      }
        catch (error) {
          console.error('Error occurred while deletion of hash attempted:', error);
      }
  }
  // it just can't come to this piece of code and execute next 2 lines, why?
     return await nrDeletedInfohashes;
  // execShellCommand("/usr/bin/bash -c \"pgrep opentracker | xargs kill --signal SIGHUP\"");
}