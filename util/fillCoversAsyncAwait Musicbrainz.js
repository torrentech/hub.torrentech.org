const mongoose = require('mongoose');
//Weird trip is that it doesn't matter if not all fields are in ./schemas/releaseArtistAlbum, find returns everything if projection is omitted
const ReleaseSchemaArtistAlbum = require('../schemas/releaseArtistAlbum');
const ReleaseModel = mongoose.model('release', ReleaseSchemaArtistAlbum);
const ReleaseDetailSchema = require('../schemas/release_detail');
const ReleaseDetailModel = mongoose.model('releases_detail', ReleaseDetailSchema);

const MusicBrainzApi = require('musicbrainz-api').MusicBrainzApi;
const mbApi = new MusicBrainzApi({
  appName: 'Torrentech.org cover filler',
  appVersion: '0.1.0',
  appContactInfo: 'domanovic@yahoo.com'
});

let allReleasesJSON;
let release;
let response;
let coverURL;
let fetchRes;
let arrayBuffer;
let buffer;
let query;
let coverUpdated;

let fetch = require('cross-fetch');

function sleep(ms) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
}

(async () => {
    try {
        await mongoose.connect("mongodb://localhost", {dbName: "Torrentech", useNewUrlParser: true, useUnifiedTopology: true});
        console.log("Connected");
    }
    catch(error) {
        console.log("Could not connect");
        console.log(error);
        process.exit(1);
    }

    try {
        allReleasesJSON = await ReleaseModel.find({}, 'release_id artist name', { lean: true });
        console.log("Releases loaded");
        console.log("_________________________________________________________________");
    }
    catch(error) {
        console.log("Can't load releases");
        console.log(error);
        process.exit(1);
    }

        // all releases or 10..........
        for (let i = 0, len = allReleasesJSON.length; i < len; ++i) {
        // for (let i = 0, len = 10; i < len; ++i) {
            try {            
                release = allReleasesJSON[i];
                console.log("Release nr: " + i);
                console.log(release);
                response = await mbApi.searchReleaseGroup({ artist: release.artist, releasegroup: release.name });
                console.log("MusicBrainz search done, release-group.id: ");
                console.log(response['release-groups'][0].id);
                try {
                    coverURL = 'https://coverartarchive.org/release-group/' + response['release-groups'][0].id + '/front-500';
                    fetchRes = await fetch(coverURL);
                    if (fetchRes.status >= 400) throw new Error("Bad response from server");
                    arrayBuffer = await fetchRes.arrayBuffer();
                    buffer = Buffer.from(arrayBuffer);
                    console.log("Cover art: ");
                    console.log(buffer);                
                    query = { release_id: release.release_id };
                    try {
                        coverUpdated = await ReleaseDetailModel.findOneAndUpdate(query, { $set: { cover: buffer }}, { useFindAndModify: false });
                        console.log(coverUpdated);
                        console.log("_________________________________________________________________");
                    } catch (error) {
                        console.log("ReleaseDetail update error: ");
                        console.log(error);
                        console.log("_________________________________________________________________");                        
                    }
    
                } catch (error) {
                    console.log("No cover art found for release: ");
                    console.log(error);
                    console.log("_________________________________________________________________");                    
                }                
            }
            catch (error) {
                console.log("mbApi error: ");
                console.log(error);
                console.log("_________________________________________________________________");                
            }
        //is await needed in front?
        await sleep(3000);
        }
})().then(() => {process.exit();});