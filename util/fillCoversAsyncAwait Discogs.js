const mongoose = require('mongoose');
//Weird trip is that it doesn't matter if not all fields are in ./schemas/releaseArtistAlbum, find returns everything if projection is omitted
const ReleaseSchemaArtistAlbum = require('../schemas/releaseArtistAlbum');
const ReleaseModel = mongoose.model('release', ReleaseSchemaArtistAlbum);
const ReleaseDetailSchema = require('../schemas/release_detail');
const ReleaseDetailModel = mongoose.model('releases_detail', ReleaseDetailSchema);

let discogsReleasesJSON;
let releaseDetailRecord;
let release;
let response;
let coverURL;
let fetchRes;
let arrayBuffer;
let buffer;
let query;
let coverUpdated;
let discogsID;

let fetch = require('cross-fetch');

function sleep(ms) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
}

(async () => {
    try {
        await mongoose.connect("mongodb://localhost", {dbName: "Torrentech", useNewUrlParser: true, useUnifiedTopology: true});
        console.log("Connected");
    }
    catch(error) {
        console.log("Could not connect");
        console.log(error);
        process.exit(1);
    }

    try {
        discogsReleasesJSON = await ReleaseModel.find({ "weblink": /discogs.com/ }, 'release_id weblink', { lean: true });
        console.log("Releases loaded");
        console.log("_________________________________________________________________");
    }
    catch(error) {
        console.log("Can't load releases");
        console.log(error);
        process.exit(1);
    }

        // all releases or 10..........
        for (let i = 0, len = discogsReleasesJSON.length; i < len; ++i) {
        // for (let i = 0, len = 6; i < len; ++i) {
            try {
                releaseDetailRecord = await ReleaseDetailModel.find({ "cover": { $type: 10 /*null*/}, "release_id": discogsReleasesJSON[i].release_id });
                // console.log("releaseDetailRecord:");
                // console.log(releaseDetailRecord.length);
                if (releaseDetailRecord.length > 0 ) {
                    try {
                        // extract disogs id
                        discogsID = discogsReleasesJSON[i].weblink.split(/[\/-]/)[4];
                        discogsID = parseInt(discogsID);
                        console.log ("release_id: " + discogsReleasesJSON[i].release_id);
                        console.log("discogsID: " + discogsID);
                        // query vns server and store cover somewhere
                        // coverURL = 'https://1e3f-37-248-253-90.ngrok.io/discogs/' + discogsID;
                        coverURL = 'http://192.168.100.2:8080/discogs/' + discogsID;
                        fetchRes = await fetch(coverURL);
                        if (fetchRes.status > 200) throw new Error("Bad response from server");
                        arrayBuffer = await fetchRes.arrayBuffer();
                        buffer = Buffer.from(arrayBuffer);
                        // console.log(buffer);
                        // write that cover into database
                        query = { release_id: discogsReleasesJSON[i].release_id };
                        coverUpdated = await ReleaseDetailModel.findOneAndUpdate(query, { $set: { cover: buffer }}, { useFindAndModify: false });
                        console.log(coverUpdated);
                        console.log('---------------------------------------------');
                        // await needed?
                        await sleep(100);
                    } catch (error) {
                        console.log("ERROR!");
                    }
                }
            } catch (error) {
                console.log("error!");
            }
        }
})().then(() => {process.exit();});