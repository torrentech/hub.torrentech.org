const path = require('path');
require("dotenv").config({ path: path.resolve(__dirname, '../.env') });
const mongoose = require('mongoose');
const ReleaseSchema = require('../schemas/release');
const ReleaseModel = mongoose.model('release', ReleaseSchema);
const fs = require('fs');
let slugify = require('slugify');
let sanitize = require("sanitize-filename");

let filename;
let artist;
let album;
let release_year;
let format;
let magnets={};
let torrentBinaryData;

function base64ToArrayBuffer(base64) {
    return Uint8Array.from(atob(base64), (v) => v.charCodeAt(0));
}

(async () => {
    try {
        await mongoose.connect(process.env.MONGO_URI, {dbName: "Torrentech", useNewUrlParser: true, useUnifiedTopology: true});
        console.log("Connected");
    }
    catch(error) {
        console.log("Could not connect");
        console.log(error);
        process.exit(1);
    }

    try {
        let myQuery;
        // with limit
        // myQuery = [ { '$lookup': { from: 'magnets', localField: 'release_id', foreignField: 'release_id', as: 'magnet' } }, { $limit: 1000  } ];
        // without limit
        myQuery = [ { '$lookup': { from: 'magnets', localField: 'release_id', foreignField: 'release_id', as: 'magnet' } } ];
        allReleasesJSON = await ReleaseModel.aggregate(myQuery);
        console.log("Releases loaded");
        console.log("_________________________________________________________________");
    }
    catch(error) {
        console.log("Can't load releases");
        console.log(error);
        process.exit(1);
    }

    for (let i = 0, len = allReleasesJSON.length; i < len; ++i) {
        release = allReleasesJSON[i];
        //console.log("Release nr: " + i);
        //console.log(release);
        artist = release.artist;
        artist = sanitize(artist);
        artist = slugify(artist);
        //not working in this case:
        //'.19.21.3.11.21.20 - Frozen-Border-11 - [2012] [MP3 320k CBR] [torrentech.org].torrent'
        if (artist.charAt(0) == '.') {
            console.log("Ima tacke");
            console.log(release);
            artist = artist.slice(1);
            console.log("artist: ");
            console.log(artist);
            console.log("--------------------------------------------------------------------------------");
        }
        album = release.name;
        album = sanitize(album);
        album = slugify(album);        
        release_year = release.release_year;
        magnets = allReleasesJSON[i].magnet;
        n = 1;
        magnets.forEach(function(magnet) {
            format = magnet.format;
            format = format.replace(" /", "");
            filename = artist + " - " + album + " - " + "[" + release_year + "] [" + format + "] [torrentech.org]-" + n +".torrent";
            n++;
            torrentBinaryData = base64ToArrayBuffer(magnet.torrent);
            fs.writeFileSync("ttdevdata/" + filename, torrentBinaryData);
        });
    }
})().then(() => {process.exit();});