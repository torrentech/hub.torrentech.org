db.releases.createIndex({ artist: 'text', name: 'text', label: 'text', tags: 'text', release_year: 'text'}, { name: 'search_index', weights: { name: 9, artist: 10, label: 8, tags: 7, release_year: 6 } });
db.releases.createIndex({ release_id: 1 }, { name: 'release_id_index' });
db.getCollection("releases_details").createIndex({ "release_id": 1 }, { name: "release_id_index", unique: true })
db.getCollection("magnets").createIndex({ "release_id": 1 }, { name: "release_id_index" })