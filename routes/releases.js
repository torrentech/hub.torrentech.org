const express = require("express");
const router = express.Router();
const cleanBody = require("../middlewares/cleanbody");
const DataController = require("../src/releases/release.controller");

router.post("/search", cleanBody, DataController.Search);
router.post("/details", cleanBody, DataController.Details);
router.post("/torrent", cleanBody, DataController.Torrent);
router.post("/tags", cleanBody, DataController.Tags);
router.post("/update", cleanBody, DataController.Update);
router.post("/updatedetails", cleanBody, DataController.UpdateDetails);
router.delete("/deleterelease", cleanBody, DataController.DeleteRelease);
router.delete("/deletemagnet", cleanBody, DataController.DeleteMagnet);

//search without pagination
//router.post("/get", cleanBody, DataController.Get);

module.exports = router;
