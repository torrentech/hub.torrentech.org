const { Schema } = require('mongoose');

module.exports = new Schema({
  user_id: {
    //   type: mongoose.Schema.Types.ObjectId,
    //   ref: 'User'
    type: Number
  },
  release_id: {
    //   type: mongoose.Schema.Types.ObjectId,
    //   ref: 'Release'
    type: Number
  },
  comment: {
      type: String
  }
},
  {
      timestamps: true
  });