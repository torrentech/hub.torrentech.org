const { Schema } = require('mongoose');

module.exports = new Schema({
  cover: {
      type: Buffer
  },
  tracklist: {
      type: String
  },
  release_id: {
    //   type: mongoose.Schema.Types.ObjectId,
    //   ref: 'Release'
    type: Number
  }
});