const { Schema } = require('mongoose');

module.exports = new Schema({
  /* user_id, username, user_level, download_credits, avatar added */
  user_id: { type: Number, unique: true },
  userId: { type: String, unique: true, required: true },
  email: { type: String, required: true, unique: true },
  username: { type: String, required: true, unique: true },
  user_level: { type: String, required: true, default: 'u', enum: ['a', 'm', 'u'] }, //"a" - administrator, "m" - moderator, "u" - user
  download_credits: { type: Number, required: true, default: 0 },
  active: { type: Boolean, default: false },
  password: { type: String, required: true },
  resetPasswordToken: { type: String, default: null },
  resetPasswordExpires: { type: Date, default: null },
  emailToken: { type: String, default: null },
  emailTokenExpires: { type: Date, default: null },
  accessToken: { type: String, default: null },
  referralCode: { type: String, unique: true },
  referrer: { type: String, default: null },
  avatar: { type: Buffer, default: null}
  },
  {
    timestamps: true
  });