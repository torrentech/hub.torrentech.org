const { Schema } = require('mongoose');
//const mongoose = require('mongoose');

module.exports = new Schema({
  release_id: {
      type: Number
  },
  artist: {
      type: String
  },
  name: {
      type: String
  },
});