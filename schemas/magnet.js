const { Schema } = require('mongoose');

module.exports = new Schema({
  magnet: {
      type: String
  },
  magnet_type: {
      type: String
  },
  torrent: {
      //type: Buffer
      //mongoimport has imported it as a String
      type: String
  },
  user_id: {
      type: Number
  },
  release_id: {
    //   type: mongoose.Schema.Types.ObjectId,
    //   ref: 'Release'
    type: Number
  },
  infohash: {
    type: String
  },
  timestamp_no_seeds: {
    type: Number,
    default: null
  },
  seeders: {
    type: Number
  },
  leechers: {
    type: Number
  }
});